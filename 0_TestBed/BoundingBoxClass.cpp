#include "BoundingBoxClass.h"
//  BoundingBoxClass
void BoundingBoxClass::Init(void)
{
	m_bInitialized = false;
	m_v3Min = vector3(0.0f);
	m_v3Max = vector3(0.0f);
	m_v3Centroid = vector3(0.0f);
	m_sName = "NULL";
}
void BoundingBoxClass::Swap(BoundingBoxClass& other)
{
	std::swap(m_bInitialized, other.m_bInitialized);
	std::swap(m_v3Min, other.m_v3Min);
	std::swap(m_v3Max, other.m_v3Max);
	std::swap(m_v3Centroid, other.m_v3Centroid);
	std::swap(m_sName, other.m_sName);
}
void BoundingBoxClass::Release(void)
{
	//No pointers to release
}
//The big 3
BoundingBoxClass::BoundingBoxClass(){Init();}
BoundingBoxClass::BoundingBoxClass(BoundingBoxClass const& other)
{
	m_bInitialized = other.m_bInitialized;
	m_v3Min = other.m_v3Min;
	m_v3Max = other.m_v3Max;
	m_v3Centroid = other.m_v3Centroid;
	m_sName = other.m_sName;
}
BoundingBoxClass& BoundingBoxClass::operator=(BoundingBoxClass const& other)
{
	if(this != &other)
	{
		Release();
		Init();
		BoundingBoxClass temp(other);
		Swap(temp);
	}
	return *this;
}
BoundingBoxClass::~BoundingBoxClass(){Release();};
//Accessors
bool BoundingBoxClass::IsInitialized(void){ return m_bInitialized; }
vector3 BoundingBoxClass::GetMinimumOBB(void){ return m_v3Min; }
vector3 BoundingBoxClass::GetMaximumOBB(void){ return m_v3Max; }
vector3 BoundingBoxClass::GetMaximumAABB(void){ return m_v3AlignedMax; }
vector3 BoundingBoxClass::GetMinimumAABB(void){ return m_v3AlignedMin; }
vector3 BoundingBoxClass::GetCentroid(void){ return m_v3Centroid; }
vector3 BoundingBoxClass::GetSize(void){ return m_v3Size; }
String BoundingBoxClass::GetName(void){return m_sName;}
//Methods
void BoundingBoxClass::GenerateOrientedBoundingBox(String a_sInstanceName)
{
	//If this has already been initialized there is nothing to do here
	if(m_bInitialized)
		return;
	MeshManagerSingleton* pMeshMngr = MeshManagerSingleton::GetInstance();
	if(pMeshMngr->IsInstanceCreated(a_sInstanceName))
	{
		m_sName = a_sInstanceName;
		
		std::vector<vector3> lVertices = pMeshMngr->GetVertices(m_sName);
		unsigned int nVertices = lVertices.size();
		m_v3Centroid = lVertices[0];
		m_v3Max = lVertices[0];
		m_v3Min = lVertices[0];
		for(unsigned int nVertex = 1; nVertex < nVertices; nVertex++)
		{
			//m_v3Centroid += lVertices[nVertex];
			if(m_v3Min.x > lVertices[nVertex].x)
				m_v3Min.x = lVertices[nVertex].x;
			else if(m_v3Max.x < lVertices[nVertex].x)
				m_v3Max.x = lVertices[nVertex].x;
			
			if(m_v3Min.y > lVertices[nVertex].y)
				m_v3Min.y = lVertices[nVertex].y;
			else if(m_v3Max.y < lVertices[nVertex].y)
				m_v3Max.y = lVertices[nVertex].y;

			if(m_v3Min.z > lVertices[nVertex].z)
				m_v3Min.z = lVertices[nVertex].z;
			else if(m_v3Max.z < lVertices[nVertex].z)
				m_v3Max.z = lVertices[nVertex].z;
		}
		m_v3Centroid = (m_v3Min + m_v3Max) / 2.0f;

		m_v3Size.x = glm::distance(vector3(m_v3Min.x, 0.0f, 0.0f), vector3(m_v3Max.x, 0.0f, 0.0f));
		m_v3Size.y = glm::distance(vector3(0.0f, m_v3Min.y, 0.0f), vector3(0.0f, m_v3Max.y, 0.0f));
		m_v3Size.z = glm::distance(vector3(0.0f, 0.0f, m_v3Min.z), vector3(0.0f, 0.0f, m_v3Max.z));

		m_bInitialized = true;
	}
}
void BoundingBoxClass::GenerateAxisAlignedBoundingBox(matrix4 a_m4ModeltoWorld)
{
	//Generate the Axis Aligned Bounding Box here based on the Oriented Bounding Box

	//reconstruct all 8 points of the bounding box
	const int nVerts = 8;
	vector3 OBBPoints[nVerts] = {};
	float xDiff = m_v3Size.x / 2;
	float yDiff = m_v3Size.y / 2;
	float zDiff = m_v3Size.z / 2;
	//+x +y +z
	OBBPoints[0] = m_v3Centroid + vector3(xDiff,yDiff,zDiff);
	//+x +y -z
	OBBPoints[1] = m_v3Centroid + vector3(xDiff,yDiff,-zDiff);
	//+x -y -z
	OBBPoints[2] = m_v3Centroid + vector3(xDiff,-yDiff,-zDiff);
	//+x -y +z	
	OBBPoints[3] = m_v3Centroid + vector3(xDiff,-yDiff,zDiff);
	//-x +y -z
	OBBPoints[4] = m_v3Centroid + vector3(-xDiff,yDiff,-zDiff);
	//-x +y +z TOP FRONT LEFT
	OBBPoints[5] = m_v3Centroid + vector3(-xDiff,yDiff,zDiff);
	//-x -y +z BOTTOM FRONT LEFT
	OBBPoints[6] = m_v3Centroid + vector3(-xDiff,-yDiff,zDiff);
	//-x -y -z
	OBBPoints[7] = m_v3Centroid + vector3(-xDiff,-yDiff,-zDiff);

	//translate all points of AABB to world space and find min/max
	for(int i= 0; i < nVerts; i++){
		vector3 toWorld = vector3(a_m4ModeltoWorld * vector4(OBBPoints[i].x,OBBPoints[i].y,OBBPoints[i].z,1));
		OBBPoints[i] = toWorld;
		//first pass, set to first value for existing point to compare against
		if(i == 0){
			m_v3AlignedMin = OBBPoints[i];
			m_v3AlignedMax = OBBPoints[i];
		}else{
			//min
			if(OBBPoints[i].x < m_v3AlignedMin.x)
				m_v3AlignedMin.x = OBBPoints[i].x;
			if(OBBPoints[i].y < m_v3AlignedMin.y)
				m_v3AlignedMin.y = OBBPoints[i].y;
			if(OBBPoints[i].z < m_v3AlignedMin.z)
				m_v3AlignedMin.z = OBBPoints[i].z;
			//max
			if(OBBPoints[i].x > m_v3AlignedMax.x)
				m_v3AlignedMax.x = OBBPoints[i].x;
			if(OBBPoints[i].y > m_v3AlignedMax.y)
				m_v3AlignedMax.y = OBBPoints[i].y;
			if(OBBPoints[i].z > m_v3AlignedMax.z)
				m_v3AlignedMax.z = OBBPoints[i].z;
		}
	}
	
	//get centroid of the aligned bounding box and store to memeber
	m_v3AlignedCentroid = (m_v3AlignedMin + m_v3AlignedMax) / 2.0f;

	//calculate the size of the aligned bounding box
	m_v3AlignedSize.x = glm::distance(vector3(m_v3AlignedMin.x, 0.0f, 0.0f), vector3(m_v3AlignedMax.x, 0.0f, 0.0f));
	m_v3AlignedSize.y = glm::distance(vector3(0.0f, m_v3AlignedMin.y, 0.0f), vector3(0.0f, m_v3AlignedMax.y, 0.0f));
	m_v3AlignedSize.z = glm::distance(vector3(0.0f, 0.0f, m_v3AlignedMin.z), vector3(0.0f, 0.0f, m_v3AlignedMax.z));

}
void BoundingBoxClass::AddBoxToRenderList(matrix4 a_m4ModelToWorld, vector3 a_vOBBColor, vector3 a_vAABBColor, bool a_bRenderCentroid)
{
	if(!m_bInitialized)
		return;
	MeshManagerSingleton* pMeshMngr = MeshManagerSingleton::GetInstance();
	if(a_bRenderCentroid)
		//pMeshMngr->AddAxisToQueue(a_m4ModelToWorld * glm::translate(m_v3Centroid));
	//add cube for axis aligned bounding box
	pMeshMngr->AddCubeToQueue(a_m4ModelToWorld * glm::translate(m_v3Centroid) * glm::scale(m_v3Size), a_vOBBColor, MERENDER::WIRE);
	

	//add AABB box
	pMeshMngr->AddCubeToQueue(glm::translate(m_v3AlignedCentroid) * glm::scale(m_v3AlignedSize), a_vAABBColor, MERENDER::WIRE);
}