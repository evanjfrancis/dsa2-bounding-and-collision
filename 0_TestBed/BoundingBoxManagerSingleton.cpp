#include "BoundingBoxManagerSingleton.h"

//  BoundingBoxManagerSingleton
BoundingBoxManagerSingleton* BoundingBoxManagerSingleton::m_pInstance = nullptr;
void BoundingBoxManagerSingleton::Init(void)
{
	m_nBoxs = 0;
}
void BoundingBoxManagerSingleton::Release(void)
{
	//Clean the list of Boxs
	for(int n = 0; n < m_nBoxs; n++)
	{
		//Make sure to release the memory of the pointers
		if(m_lBox[n] != nullptr)
		{
			delete m_lBox[n];
			m_lBox[n] = nullptr;
		}
	}
	m_lBox.clear();
	m_lMatrix.clear();
	m_lOBBColor.clear();
	m_lAABBColor.clear();
	m_nBoxs = 0;
}
BoundingBoxManagerSingleton* BoundingBoxManagerSingleton::GetInstance()
{
	if(m_pInstance == nullptr)
	{
		m_pInstance = new BoundingBoxManagerSingleton();
	}
	return m_pInstance;
}
void BoundingBoxManagerSingleton::ReleaseInstance()
{
	if(m_pInstance != nullptr)
	{
		delete m_pInstance;
		m_pInstance = nullptr;
	}
}
//The big 3
BoundingBoxManagerSingleton::BoundingBoxManagerSingleton(){Init();}
BoundingBoxManagerSingleton::BoundingBoxManagerSingleton(BoundingBoxManagerSingleton const& other){ }
BoundingBoxManagerSingleton& BoundingBoxManagerSingleton::operator=(BoundingBoxManagerSingleton const& other) { return *this; }
BoundingBoxManagerSingleton::~BoundingBoxManagerSingleton(){Release();};
//Accessors
int BoundingBoxManagerSingleton::GetBoxTotal(void){ return m_nBoxs; }

//--- Non Standard Singleton Methods
void BoundingBoxManagerSingleton::GenerateBoundingBox(matrix4 a_mModelToWorld, String a_sInstanceName)
{
	MeshManagerSingleton* pMeshMngr = MeshManagerSingleton::GetInstance();
	//Verify the instance is loaded
	if(pMeshMngr->IsInstanceCreated(a_sInstanceName))
	{//if it is check if the Box has already been created
		int nBox = IdentifyBox(a_sInstanceName);
		if(nBox == -1)
		{
			//Create a new bounding Box
			BoundingBoxClass* pBB = new BoundingBoxClass();
			//construct its information out of the instance name
			pBB->GenerateOrientedBoundingBox(a_sInstanceName);
			//Push the Box back into the list
			m_lBox.push_back(pBB);
			//Push a new matrix into the list
			m_lMatrix.push_back(matrix4(IDENTITY));
			//Specify the color the Box is going to have
			m_lOBBColor.push_back(vector3(1.0f));
			m_lAABBColor.push_back(vector3(0.f,0.f,1.f));
			//Increase the number of Boxes
			m_nBoxs++;
		}
		else //If the box has already been created you will need to check its global orientation
		{
			m_lBox[nBox]->GenerateAxisAlignedBoundingBox(a_mModelToWorld);
		}
		nBox = IdentifyBox(a_sInstanceName);
		m_lMatrix[nBox] = a_mModelToWorld;
	}
}

void BoundingBoxManagerSingleton::SetBoundingBoxSpace(matrix4 a_mModelToWorld, String a_sInstanceName)
{
	int nBox = IdentifyBox(a_sInstanceName);
	//If the Box was found
	if(nBox != -1)
	{
		//Set up the new matrix in the appropriate index
		m_lMatrix[nBox] = a_mModelToWorld;
	}
}

int BoundingBoxManagerSingleton::IdentifyBox(String a_sInstanceName)
{
	//Go one by one for all the Boxs in the list
	for(int nBox = 0; nBox < m_nBoxs; nBox++)
	{
		//If the current Box is the one we are looking for we return the index
		if(a_sInstanceName == m_lBox[nBox]->GetName())
			return nBox;
	}
	return -1;//couldn't find it return with no index
}

void BoundingBoxManagerSingleton::AddBoxToRenderList(String a_sInstanceName)
{
	//If I need to render all
	if(a_sInstanceName == "ALL")
	{
		for(int nBox = 0; nBox < m_nBoxs; nBox++)
		{
			m_lBox[nBox]->AddBoxToRenderList(m_lMatrix[nBox], m_lOBBColor[nBox], m_lAABBColor[nBox], true);
		}
	}
	else
	{
		int nBox = IdentifyBox(a_sInstanceName);
		if(nBox != -1)
		{
			m_lBox[nBox]->AddBoxToRenderList(m_lMatrix[nBox], m_lOBBColor[nBox], m_lAABBColor[nBox], true);
		}
	}
}

void BoundingBoxManagerSingleton::CalculateCollision(void)
{
	//Create a placeholder for all center points
	std::vector<vector3> lCentroid;
	//for all Boxs...
	for(int nBox = 0; nBox < m_nBoxs; nBox++)
	{
		//Make all the Boxs white
		m_lOBBColor[nBox] = vector3(1.0f);
		m_lAABBColor[nBox] = vector3(0.f,0.f,1.f);
		//Place all the centroids of Boxs in global space
		lCentroid.push_back(static_cast<vector3>(m_lMatrix[nBox] * vector4(m_lBox[nBox]->GetCentroid(), 1.0f)));
	}
	//Oriented boxes
	//Now the actual check
	/*for(int i = 0; i < m_nBoxs - 1; i++)
	{
		for(int j = i + 1; j < m_nBoxs; j++)
		{
			//If the distance between the center of both Boxs is less than the sum of their radius there is a collision
			//For this check we will assume they will be colliding unless they are not in the same space in X, Y or Z
			//so we place them in global positions
			vector3 v1Min = static_cast<vector3>(m_lMatrix[i] * vector4(m_lBox[i]->GetMinimumOBB(),1));
			vector3 v1Max = static_cast<vector3>(m_lMatrix[i] * vector4(m_lBox[i]->GetMaximumOBB(),1));

			vector3 v2Min = static_cast<vector3>(m_lMatrix[j] * vector4(m_lBox[j]->GetMinimumOBB(),1));
			vector3 v2Max = static_cast<vector3>(m_lMatrix[j] * vector4(m_lBox[j]->GetMaximumOBB(),1));

			bool bColliding = true;
			if(v1Max.x < v2Min.x || v1Min.x > v2Max.x)
				bColliding = false;
			else if(v1Max.y < v2Min.y || v1Min.y > v2Max.y)
				bColliding = false;
			else if(v1Max.z < v2Min.z || v1Min.z > v2Max.z)
				bColliding = false;

			if(bColliding)
				m_lOBBColor[i] = m_lOBBColor[j] = MERED; //We make the Boxes red
		}
	}*/

	//check Axis Aligned boxes first
	for(int i = 0; i < m_nBoxs - 1; i++)
	{
		for(int j = i + 1; j < m_nBoxs; j++)
		{
			//If the distance between the center of both Boxs is less than the sum of their radius there is a collision
			//For this check we will assume they will be colliding unless they are not in the same space in X, Y or Z
			//so we place them in global positions
			vector3 v1Min = m_lBox[i]->GetMinimumAABB();
			vector3 v1Max = m_lBox[i]->GetMaximumAABB();

			vector3 v2Min = m_lBox[j]->GetMinimumAABB();
			vector3 v2Max = m_lBox[j]->GetMaximumAABB();


			bool bAABBColliding = true;
			if(v1Max.x < v2Min.x || v1Min.x > v2Max.x)
				bAABBColliding = false;
			else if(v1Max.y < v2Min.y || v1Min.y > v2Max.y)
				bAABBColliding = false;
			else if(v1Max.z < v2Min.z || v1Min.z > v2Max.z)
				bAABBColliding = false;

			//if AABB collided, check for Separation Axis Test using OBB
			if(bAABBColliding){
				//change color
				m_lAABBColor[i] = m_lAABBColor[j] = MEYELLOW; //We make the Boxes yellow if AABB collide
				
				if(TestSATCollision(i,j)){
					m_lOBBColor[i] = m_lOBBColor[j] = MERED;
				}
			}
		}
	}
}

bool BoundingBoxManagerSingleton::TestSATCollision(int i, int j)
{
	/*
		NOTE: the reference code is not valid, it's just for an example. 
		The example uses an "OBB" struct, this is how it relates to our code - Evan
					
		OBB.e - vector3 half size of OBB
			e.x = x half width 
			e.y = y half width
			e.z = z half width
		OBB.u - vector3 local axis
			u.x = local x axis
			u.y = local y axis
			u.z = local z axis
		OBB.c - vector3 center point
			c - center point location

		So for translating the reference to our code...
			a.u[2]	--> v3ALocalAxis[2]
			a.c		--> v3ACentroid
			a.e		--> v3AHalfWidth
	*/

	//perform Separation Axis Test
	float ra, rb;
	glm::mat3 m3R, m3AbsR;

	//store local axis of each in world space
	vector4 v4XAxis = vector4(1.f,0.f,0.f,0.f);
	vector4 v4YAxis = vector4(0.f,1.f,0.f,0.f);
	vector4 v4ZAxis = vector4(0.f,0.f,1.f,0.f);
	vector3 v3ALocalAxis[3];
	v3ALocalAxis[0] = vector3(m_lMatrix[i] * v4XAxis);
	v3ALocalAxis[1] = vector3(m_lMatrix[i] * v4YAxis);
	v3ALocalAxis[2] = vector3(m_lMatrix[i] * v4ZAxis);
	vector3 v3BLocalAxis[3];
	v3BLocalAxis[0] = vector3(m_lMatrix[j] * v4XAxis);
	v3BLocalAxis[1] = vector3(m_lMatrix[j] * v4YAxis);
	v3BLocalAxis[2] = vector3(m_lMatrix[j] * v4ZAxis);
				
	//store halfwidth extents of OBB
	vector3 v3AHalfWidth = m_lBox[i]->GetSize()/2.0f;
	vector3 v3BHalfWidth = m_lBox[j]->GetSize()/2.0f;

	//store centroid in world space
	vector3 v3ACentroidLocal = m_lBox[i]->GetCentroid();
	vector3 v3BCentroidLocal = m_lBox[j]->GetCentroid();
	vector3 v3ACentroid = vector3(m_lMatrix[i] * vector4(v3ACentroidLocal, 1.f));
	vector3 v3BCentroid = vector3(m_lMatrix[j] * vector4(v3BCentroidLocal, 1.f));

	// Compute rotation matrix expressing b in a�s coordinate frame
	for (int ii = 0; ii < 3; ii++){
		for (int jj = 0; jj < 3; jj++){
			//R[i][j] = Dot(a.u[i], b.u[j]);
			m3R[ii][jj] = glm::dot(v3ALocalAxis[ii], v3BLocalAxis[jj]);
		}
	}


	// Compute translation vector t
	//Vector t = b.c - a.c;
	vector3 v3Trans = v3BCentroid - v3ACentroid;
						
	// Bring translation into a�s coordinate frame
	//t = Vector(Dot(t, a.u[0]), Dot(t, a.u[2]), Dot(t, a.u[2]));
	v3Trans = vector3(glm::dot(v3Trans, v3ALocalAxis[0]), glm::dot(v3Trans, v3ALocalAxis[1]), glm::dot(v3Trans, v3ALocalAxis[2]));

	// Compute common subexpressions. Add in an epsilon term to
	// counteract arithmetic errors when two edges are parallel and
	// their cross product is (near) null (see text for details)
	for (int ii = 0; ii < 3; ii++){
		for (int jj = 0; jj < 3; jj++){
			//AbsR[i][j] = abs(R[i][j]) + EPSILON; 
			m3AbsR[ii][jj] = abs(m3R[ii][jj]) + std::numeric_limits<float>::epsilon();
		}
	}
						
	// Test axes L = A0, L = A1, L = A2
	for (int ii = 0; ii < 3; ii++) {
		//ra = v3AHalfWidth[i];
		//rb = v3BHalfWidth[0] * m3AbsR[i][0] + v3BHalfWidth[1] * m3AbsR[i][1] + v3BHalfWidth[2] * m3AbsR[i][2];
		//if (abs(v3Trans[i]) > ( ra + rb )) 
		//	return 0;
		ra = v3AHalfWidth[ii];
		rb = v3BHalfWidth[0] * m3AbsR[ii][0] + v3BHalfWidth[1] * m3AbsR[ii][1] + v3BHalfWidth[2] * m3AbsR[ii][2];
		if(abs(v3Trans[ii]) > (ra + rb))
			return false;
	}
						
	// Test axes L = B0, L = B1, L = B2
	for (int ii = 0; ii < 3; ii++) {
		//ra = v3AHalfWidth[0] * m3AbsR[0][i] + v3AHalfWidth[1] * m3AbsR[1][i] + v3AHalfWidth[2] * m3AbsR[2][i];
		//rb = v3BHalfWidth[i];
		//if (abs(v3Trans[0] * m3R[0][i] + v3Trans[1] * m3R[1][i] + v3Trans[2] * m3R[2][i]) > ( ra + rb )) 
		//	return 0;
		ra = v3AHalfWidth[0] * m3AbsR[0][ii] + v3AHalfWidth[1] * m3AbsR[1][ii] + v3AHalfWidth[2] * m3AbsR[2][ii];
		rb = v3BHalfWidth[ii];
		if(abs(v3Trans[0] * m3R[0][ii] + v3Trans[1] * m3R[1][ii] + v3Trans[2] * m3R[2][ii]) > (ra + rb))
			return false;
	}

	// Test axis L = A0 x B0
	//ra = v3AHalfWidth[1] * m3AbsR[2][0] + v3AHalfWidth[2] * m3AbsR[1][0];
	//rb = v3BHalfWidth[1] * m3AbsR[0][2] + v3BHalfWidth[2] * m3AbsR[0][1];
	//if (abs(v3Trans[2] * m3R[1][0] - v3Trans[1] * m3R[2][0]) > ( ra + rb )) 
		//return 0;
	ra = v3AHalfWidth[1] * m3AbsR[2][0] + v3AHalfWidth[2] * m3AbsR[1][0];
	rb = v3BHalfWidth[1] * m3AbsR[0][2] + v3BHalfWidth[2] * m3AbsR[0][1];
	if(abs(v3Trans[2] * m3R[1][0] - v3Trans[1] * m3R[2][0]) > (ra + rb))
		return false;
						
	// Test axis L = A0 x B1
	//ra = v3AHalfWidth[1] * m3AbsR[2][1] + v3AHalfWidth[2] * m3AbsR[1][1];
	//rb = v3BHalfWidth[0] * m3AbsR[0][2] + v3BHalfWidth[2] * m3AbsR[0][0];
	//if (abs(v3Trans[2] * m3R[1][1] - v3Trans[1] * m3R[2][1]) > ( ra + rb )) 
	//	return 0;
	ra = v3AHalfWidth[1] * m3AbsR[2][1] + v3AHalfWidth[2] * m3AbsR[1][1];
	rb = v3BHalfWidth[0] * m3AbsR[0][2] + v3BHalfWidth[2] * m3AbsR[0][0];
	if(abs(v3Trans[2] * m3R[1][1] - v3Trans[1] * m3R[2][1]) > (ra + rb))
		return false;
						
	// Test axis L = A0 x B2
	//ra = v3AHalfWidth[1] * m3AbsR[2][2] + v3AHalfWidth[2] * m3AbsR[1][2];
	//rb = v3BHalfWidth[0] * m3AbsR[0][1] + v3BHalfWidth[1] * m3AbsR[0][0];
	//if (abs(v3Trans[2] * m3R[1][2] - v3Trans[1] * m3R[2][2]) > ( ra + rb ))
	//	return 0;
	ra = v3AHalfWidth[1] * m3AbsR[2][2] + v3AHalfWidth[2] * m3AbsR[1][2];
	rb = v3BHalfWidth[0] * m3AbsR[0][1] + v3BHalfWidth[1] * m3AbsR[0][0];
	if(abs(v3Trans[2] * m3R[1][2] - v3Trans[1] * m3R[2][2]) > (ra + rb))
		return false;
						
	// Test axis L = A1 x B0
	ra = v3AHalfWidth[0] * m3AbsR[2][0] + v3AHalfWidth[2] * m3AbsR[0][0];
	rb = v3BHalfWidth[1] * m3AbsR[1][2] + v3BHalfWidth[2] * m3AbsR[1][1];
	if (abs(v3Trans[0] * m3R[2][0] - v3Trans[2] * m3R[0][0]) > ( ra + rb )) 
		return false;


	// Test axis L = A1 x B1
	ra = v3AHalfWidth[0] * m3AbsR[2][1] + v3AHalfWidth[2] * m3AbsR[0][1];
	rb = v3BHalfWidth[0] * m3AbsR[1][2] + v3BHalfWidth[2] * m3AbsR[1][0];
	if (abs(v3Trans[0] * m3R[2][1] - v3Trans[2] * m3R[0][1]) > ( ra + rb )) 
		return false;


	// Test axis L = A1 x B2
	ra = v3AHalfWidth[0] * m3AbsR[2][2] + v3AHalfWidth[2] * m3AbsR[0][2];
	rb = v3BHalfWidth[0] * m3AbsR[1][1] + v3BHalfWidth[1] * m3AbsR[1][0];
	if (abs(v3Trans[0] * m3R[2][2] - v3Trans[2] * m3R[0][2]) > ( ra + rb )) 
		return false;


	// Test axis L = A2 x B0
	ra = v3AHalfWidth[0] * m3AbsR[1][0] + v3AHalfWidth[1] * m3AbsR[0][0];
	rb = v3BHalfWidth[1] * m3AbsR[2][2] + v3BHalfWidth[2] * m3AbsR[2][1];
	if (abs(v3Trans[1] * m3R[0][0] - v3Trans[0] * m3R[1][0]) > ( ra + rb )) 
		return false;


	// Test axis L = A2 x B1
	ra = v3AHalfWidth[0] * m3AbsR[1][1] + v3AHalfWidth[1] * m3AbsR[0][1];
	rb = v3BHalfWidth[0] * m3AbsR[2][2] + v3BHalfWidth[2] * m3AbsR[2][0];
	if (abs(v3Trans[1] * m3R[0][1] - v3Trans[0] * m3R[1][1]) > ( ra + rb )) 
		return false;

	// Test axis L = A2 x B2
	ra = v3AHalfWidth[0] * m3AbsR[1][2] + v3AHalfWidth[1] * m3AbsR[0][2];
	rb = v3BHalfWidth[0] * m3AbsR[2][1] + v3BHalfWidth[1] * m3AbsR[2][0];
	if (abs(v3Trans[1] * m3R[0][2] - v3Trans[0] * m3R[1][2]) > ( ra + rb )) 
		return false;
				

	return true;
			
}