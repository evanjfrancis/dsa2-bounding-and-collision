# A10 Separating Axis Test #
 
 By Evan Francis (ejf1872) and Thomas Farrell (tjf1134)
 
** Evan's contributions: ** 

 * added separating axis test algorithm by following the logic in the book
 * made SAT only check if AABB detects collision
 * pulled SAT code out into its own method, TestSATCollision()
 
** Tom's contributions: **

 * added GetSize to BoundingBoxClass.cpp/.h
 * General Debugging
